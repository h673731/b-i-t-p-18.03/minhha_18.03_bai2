package org.example;

public class Dogs extends Animals{
    public Dogs(String name, int numberLeg, String color) {
        super(name, numberLeg, color);
    }

    @Override
    public void sounding() {
        System.out.println("Dogs");
    }

    public void sounding(String origin) {
        System.out.println(origin);
    }
}
