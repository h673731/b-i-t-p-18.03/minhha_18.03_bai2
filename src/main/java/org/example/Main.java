package org.example;

public class Main {
    public static void main(String[] args) {
        Animals animals = new Animals("Dong vat", 2, "den");
        Dogs dogs = new Dogs("Shiba", 4, "cam");
        Animals cuong = new Dogs("Cuong", 2, "pamu");
        cuong.sounding();
        dogs.sounding();
        dogs.sounding("Vietnam");
        animals.sounding();
        String name = animals.name;

        //Chỉ gọi được phương thức sound() trong Animals do sử dụng public
        //Ko gọi đến được các thuộc tính của Animals do sử dụng private và ko có getter, setter
        //Khi class cha có 1 constructor có tham số, class con extend class cha thì class con ko thể tự tạp ra 1 constructor ko tham số và phải khai báo 1 constructor và dùng từ khóa (super) để tham chiếu sang các thuộc tính của class cha
        //Do đó để tránh trường hợp này, class cha phải tạo 1 constructor có đầy đủ tham số và 1 constructor ko có tham số
    }
}